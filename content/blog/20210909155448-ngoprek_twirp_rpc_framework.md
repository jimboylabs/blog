+++
title = "Ngoprek Twirp RPC Framework"
author = ["Wayanjimmy"]
publishDate = 2021-09-09
draft = false
+++

## Sedikit konteks {#sedikit-konteks}

Terinspirasi dari talk di channel [Insinyur Online](https://youtu.be/DR0yj7yEDdw) tentang [Pursuit of Production Ready Software](https://braindump.wayanjimmy.xyz/notes/20210902084025-bedah%5Fpraktek%5F000%5Fpursuit%5Fof%5Fproduction%5Fready%5Fsoftware/), saya terdorong untuk memulai sesi oprek dan mendokumentasikan-nya dalam beberapa media, salah satunya tulisan ini. (ini cuap cuap saja, sebenarnya saya tertarik hadiah buku-nya wqwq).

{{< figure src="/ox-hugo/pursuing-production-ready.png" >}}

Salah satu topik yang sedang saya pelajari adalah tentang Sistem Terdistribusi, dan masih jadi, misteri buat saya gimana caranya membuat komunikasi antar aplikasi yang _reliable_, _scalable_ dan _maintanable_ (sesuai dengan 3 attributes membuat aplikasi siap guna).

Tapi saya tidak akan membahas dari segi 3 attributes diatas, karena saya ada masalah _lain_ yang ingin telusuri.


## Permasalahan {#permasalahan}

Saat proyek baru dimulai, mengembangkan komunikasi aplikasi sistem terdistribusi dengan _REST API_ mungkin belum muncul banyak masalah, tapi masalah mulai terasa ketika _API_ endpoint terus bertambah seiring penambahan fitur, Pengembang yang terlibat juga bertambah seiring perkembangan organisasi.

Sehingga setiap menulis sebuah endpoint baru, muncul pertanyaan sbb.

-   Data apa yang harus dikembalikan?
-   Bagaimana bentuk data yang dikembalikan?
-   Bagaimana mengkomunikasi-kan error?
-   Headers apa saja yang perlu dikembalikan?
-   Bagaimana mengatur versioning?
-   dan banyak lagi.....wqwq

Belum ada-nya standarisasi menambah **runyam** kondisi ini, jadi saya mencari sebuah petunjuk, dan menemukan sebuah konsep yang bernama _Remote Procedural Call_ selanjutnya disebut RPC[^fn:1]. Saya juga breakdown masalah ini dalam bentuk [mindmap](https://www.mindomo.com/mindmap/twirp-rpc-framework-cd369c82c93d457ba70a66327f01dd37).


## Apa itu RPC? {#apa-itu-rpc}

Yang saya tangkap, RPC ini adalah sebuah fungsi yang sebenarnya **di-eksekusi** di **mesin berbeda** dimana fungsi ini dipanggil, namun karena RPC sudah **menyembunyikan** implementasi-nya, kita seolah-olah **memanggil** fungsi tersebut dari fungsi **utama**.

Karena sedang belajar [Golang](https://braindump.wayanjimmy.xyz/notes/20201205165502-golang/), saya mencari sebuah implementasi RPC dan menemukan [Twirp](https://github.com/twitchtv/twirp). Module ini menurut saya menarik karena dibuat oleh Twitch dan sudah digunakan secara _production_, katanya sih begitu, mari kita gali lagi.


### Twirp sebuah Framework RPC ditulis dengan Golang {#twirp-sebuah-framework-rpc-ditulis-dengan-golang}

Mengapa memilih Twirp, dari tulisan blog[^fn:2] ini saya menemukan beberapa hal menarik sbb.

-   RPC terstruktur lebih mudah di design dan maintain daripada REST API yang berorientasi URL. (ini saya relate banget)
-   Penulis blog post ini mengatakan, yang diperlukan adalah standarisasi bentuk URL, bentuk request, response dan error messages. (wah pas banget, sesuai yang saya cari)
-   Bisa jalan di HTTP 1.1 dan support JSON serialization, _legacy_ project pastinya menyukai hal ini, tidak perlu takut menghadapi keribetan menggunakan _grpc_, yang saya sendiri belum tahu itu apa.


## Bagaimana caranya menggunakan Twirp? {#bagaimana-caranya-menggunakan-twirp}

Output dari sesi oprek kali ini adalah menghasilkan _proof of concept_ sbb.

1.  Membuat implementasi client dan server menggunakan definisi protobuf dan Twirp
2.  Membuat implementasi client dari browser

Lebih detail tentang langkah-langkah oprekan ini, saya rekam dalam catatan yg lebih kasar [disini](https://braindump.wayanjimmy.xyz/notes/20210811091303-twirp%5Frpc%5Fframework/).


## Kesimpulan {#kesimpulan}


### Pro {#pro}

-   Mengurangi menulis definisi request dan response sebuah aplikasi dengan manual, cukup tulis protobuf definition dan generate code-nya
-   Tersedia generator untuk berbagai bahasa seperti Javascript dan Typescript
-   Support JSON memudahkan proses debugging
-   Menurut saya bisa jadi alternatif teknologi seperti GraphQL
-   Definisi protobuf mudah di export ke format [OpenAPI](https://github.com/wayanjimmy/twirp-grpc-envoy-poc/blob/main/doc%5Fgen.sh), ini berguna buat bikin dokumentasi.


### Kontra {#kontra}

-   Belum ada pengalaman menggunakan-nya di aplikasi siap guna / production ready
-   Masih belum yakin bagaimana strategi adopsinya pada project yang multi-repo


## Catatan Kaki {#catatan-kaki}

[^fn:1]: [Wikipedia: RPC](https://en.wikipedia.org/wiki/Remote%5Fprocedure%5Fcall)
[^fn:2]: [Twirp a Sweet new RPC Framework for Go](https://blog.twitch.tv/en/2018/01/16/twirp-a-sweet-new-rpc-framework-for-go-5f2febbf35f/)
