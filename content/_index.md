# Blog-nya Wayan Jimmy

Halo! aku Jimmy, disini tempatku untuk belajar menjadikan nulis sebagai _habit_, berusaha menyeimbangkan proses konsumsi informasi dan menghasilkan informasi.

Tulisan di blog ini, dibagi jadi 3 kategori:

1. [Fragment](/fragment), potongan pemikiran kecil yang ditulis dengan kata-kata sendiri.

1. [Blog](/blog), potongan pemikiran yang lebih panjang, biasanya hasil relasi antar Fragment, [Braindump](https://braindump.wayanjimmy.xyz) disertai dengan daftar tautan referensi.
