+++
title = "Bagaimana cara menggunakan Otak?"
author = ["Wayanjimmy"]
publishDate = 2021-02-17
draft = false
+++

## Bagaimana cara menggunakan Otak? {#bagaimana-cara-menggunakan-otak}

Semakin lama teknologi semakin canggih dan memanjakan pengguna-nya. Hal
ini berbahaya karena membuat pengguna-nya menjadi lupa akan kemampuan
dirinya sendiri.

Salah satu contoh adalah _Smartphone_, dari namanya sudah mencerminkan
sesuatu yang pintar, sayangnya banyak orang yang belum bisa
mengoptimalkan penggunaannya. Alat ini berbalik jadi alat yg memproduksi
**kecemasan** karena manusia akan mulai membandingkan dirinya sendiri
dengan orang lain.

Contoh yang paling dekat adalah _Otak_, bahkan kesadaran manusia sampai
sekarang masih menjadi misteri. Hal ini diperburuk dengan hadirnya
_Smartphone_, manusia semakin tidak mengenali kemampuan dirinya sendiri
untuk bernalar, berubah jadi makhluk yg kurang berakal.

Selain itu Negara pun tidak memfasilitasi penduduknya untuk melatih
otaknya. Sistem pendidikan yg seharusnya berperan dalam hal ini, berubah
menjadi sistem pembodohan masal. Kenapa? karena menekankan pada proses
menghafal. Tugas yang serba melibatkan komputer, terkesan melek
teknologi, ujung-ujungnya hanya proses duplikat, kumpul dan mengejar
nilai.

Sedangkan hidup yg sebenarnya akan dihadapi sangat dinamis, jauh dari
**hafalan**, setiap tindakan memerlukan nalar dan kemampuan adaptasi agar
tindakan kita tidak mengganggu Hak orang lain.

Pertanyaan selanjutnya adalah Bagaimana cara menggunakan _Otak_ kita?
