+++
title = "Bahasa jurang ekspresi"
author = ["Wayanjimmy"]
publishDate = 2021-02-17
draft = false
+++

## Bahasa jurang ekspresi {#bahasa-jurang-ekspresi}

Bisa membantu orang lain tanpa memikirkan timbal balik, bagiku adalah
salah satu cara bahagia.

Namun ketika membalas ucapan terima kasih seseorang, aku merasa seperti
kehabisan kosa kata yang mewakili. Sesekali otak ini malas berpikir dan
ucapan yang keluar sekadar _Santai aja, Gapapa kok_.

Menyampaikan apa yang aku rasain ke orang lain, adalah salah satu hal yg
pengen aku pelajari dan perbaiki. Jadi agar orang lain menangkap apa
yang aku rasakan, aku mencari kata-kata yg lebih baik.

_I'm glad I was able to help_, kata itu yg muncul pertama dikepala, tapi
kenapa yg muncul bahasa Inggris?, mungkin media yang aku konsumsi
belakangan sering memunculkan kalimat tersebut.

Ketika di terjemahkan ke Bahasa Indonesia akan jadi, "Senang bisa
membantu" Ok, untuk belajar ini aku harus terbiasa mengerahkan tenaga
tambahan untuk memikirkan padanan kata yg hampir mewakili apa yang
kurasakan.
