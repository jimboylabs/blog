+++
title = "Leluhur sebagai bahan menebar ketakutan"
author = ["Wayanjimmy"]
publishDate = 2021-09-22
draft = false
+++

## Leluhur sebagai bahan menebar ketakutan {#leluhur-sebagai-bahan-menebar-ketakutan}

Tidak jarang saya dengar kalimat ini melewati telinga saya.

Jika kamu tidak (menikah, punya anak), artinya kamu tidak melaksanakan kewajiban [Caturasrama](https://id.wikipedia.org/wiki/Caturasrama), yang mana akan membuat leluhur murka.

Jika kamu tidak rajin sembahyang, leluhur akan murka.


### Bagaimana jika leluhur Murka? {#bagaimana-jika-leluhur-murka}

Sering kali ditakuti dengan hal-hal lain seperti:

Selama hidupmu kamu tidak akan sukses. Sebelum mengartikan pernyataan ini, ada pertanyaan yang sebaiknya dijawab terlebih dahulu, Apa itu Sukses? sebagian besar saya yakin sukses disini bersifat materialistis, padahal penyebab tidak sukses karena tidak bisa mengatur [keuangan](https://braindump.wayanjimmy.xyz/notes/20210425134255-finance/).

Selama hidupmu kamu akan diberi penyakit. Penyakit bisa saja timbul karena gaya hidup tidak sehat yang kita anggap sebagai hal yang biasa. Makan terlalu banyak, jarang bergerak dan berolahraga.


### Leluhur selalu mendampingi kita {#leluhur-selalu-mendampingi-kita}

Daripada hidup dalam [ketakutan](https://youtu.be/MtimAuhyP-M) akan kemurkaan Leluhur, akan lebih baik menurut saya hidup dalam damai dan percaya jika leluhur selalu mendampingi, apapun langkah yang saya ambil, banyak hal dalam hidup ini yang terkesan keberuntungan bagi saya.

Tentu saja keberuntungan tidak didapat begitu saja, tetapi dengan kerja keras, kerja keras akan meningkatkan kemungkinan mendapat keberuntungan dan Beliau dengan senang hati mendukung kita dalam situasi dan kondisi seperti apapun.
